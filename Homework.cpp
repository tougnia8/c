﻿// Test2.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>
using namespace std;

int main() {
	setlocale(LC_ALL, "Russian");

	string name("Дима");
	cout << "Мое имя: " << name << "\n Длина строки: " << name.length()
		<< "\n Первый символ строки: " << name.front() 
		<< "\n Последний символ строки: " << name.back();
}
