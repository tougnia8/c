#include <iostream>
#include <vector>
#include <string>
using namespace std;
template<typename T>
class Stack
{
public:
    Stack(size_t _size = 20) {
        size = _size;
        topIndex = 0;
        stack = new T[size];
    }
    ~Stack() {
        delete[] stack;
    }
    void resize() {
        T new_size = size * 2;
        T * new_stack;
        new_stack = new T[new_size];
        for (size_t i = 0; i < size; ++i) {
            new_stack[i] = stack[i];
        }
        delete[] stack;
        stack = new_stack;
        size = new_size;
    }
    void print(){
      for (int i = 0; i <= topIndex - 1; i++)
          cout  << stack[i] << endl;
    }
    void push(T elem) {
        if (topIndex + 1 == size) {
            resize();
        }
        stack[topIndex++] = elem;
    }
    void pop(){
        if ( topIndex <= 0 ){
            throw std::out_of_range("stack is empty");
        } else {
            topIndex--;
        }
    }
    int top() {
        if (topIndex <= 0) {
            throw std::out_of_range("stack is empty");
        }
        else {
            return stack[topIndex - 1];
        }
    }
private:
    size_t size;
    size_t topIndex;
    T* stack;
};
int main() {
    Stack<int> num;
    for (int i = 0; i < 10; i++) {
        num.push(i);
    }
    num.print();
    for (int i = 0; i < 5; ++i) {
        num.pop();
    }
    num.print();

  Stack<char> world;
  char ch;

   for (int i = 0; i < 2; i++) {
       cin >> ch;
       world.push(ch);
   }
   world.print();
    return 0;
}
