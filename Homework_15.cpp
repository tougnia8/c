#include <iostream>

using namespace std;
int const N = 10;
void FindOddEvenNumber(int limit, bool odd) {
    // a = 0 Чётные числа a = 1 Нечётные числа
    // N количество чисел
    for (int x = 0; x < limit; x++) {
        if (x % 2 == odd) {
           cout << x << "\n";
        }
    }
}
int main() {
    // Вывод чётных чисел по константе N
    for (int x = 0; x < N; x++) {
        if (x % 2 == 0) {
            cout << x << "\n";
        }
    }
    FindOddEvenNumber(100, 1);
}
