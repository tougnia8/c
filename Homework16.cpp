#define _CRT_SECURE_NO_WARNINGS
#include <iostream>

#include <ctime>

int const N = 10;
using namespace std;
int main() {
	int Array[N][N];
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			Array[i][j] = i + j;
			cout << Array[i][j];
		}
		cout << "\n";
	}
	int sum = 0;
	time_t t;
	time(&t);
	int i = (localtime(&t)->tm_mday) % N;
		for (int j = 0; j < N; j++) {
			sum += Array[i][j];
		}
		cout << sum << "\n";
};
