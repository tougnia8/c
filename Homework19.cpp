#include <iostream>
#include <string>
using namespace std;

class Animal {
public:
	void virtual Voices() {
		cout << "" << endl;
	}
};
class Dog : public Animal {
public:
	void Voices() override {
		cout << "Woof!" << endl;
	}
};
class Cat : public Animal {
public:
	void Voices() override {
		cout << "Meow!" << endl;
	}
};
class Cow : public Animal {
public:
	void Voices() override {
		cout << "Moo!" << endl;
	}
};

int main() {
	Animal** p = new Animal*[3];
	p[0] = new Dog;
	p[1] = new Cat;
	p[2] = new Cow;
	for (int i = 0; i < 3; i++) {
		p[i]->Voices();
	}
	for (int i = 0; i < 2; i++) {
		delete p[i];
	}
	delete [] p;
	return 0;
}